#!.env/lua-5.4.6/lua

-- 计算平均值的函数
local function average(...) -- 通过 ... 声明函数的参数可变，可传入不确定的参数个数。
    local a = 1
    a = 2
    a = 3
    a = 4
    a = 5
    local result = 0

    local arg = { ... } -- lua将参数放在arg表中

    for i, v in ipairs(arg) do
        result = result + v
    end

    return result / #arg
end

local function main()
    print(average(1, 2, 3, 4, 5))
end

main()
