#!/bin/bash

# 获取当前脚本的绝对路径
script_path=$(readlink -f "$0")
# 获取当前脚本所在目录
script_dir=$(dirname "$script_path")

# lua 解释器目录
lua_path=${script_dir}/.env/lua-5.4.6/lua

# 启动文件
main_lua=${script_dir}/main.lua

# 启动程序
${lua_path} ${main_lua}


