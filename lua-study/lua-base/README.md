# 参考资料

## 博客

[VSCode 配置 Lua 开发环境（清晰明了）](https://blog.csdn.net/qq_44697754/article/details/133103543)

[看完就会vscode+Lua断点调试，不在苦于Unity+Lua没得断点的苦恼](https://blog.csdn.net/qq_28820675/article/details/105442889)

[超级详细的Lua语言的基础教程](https://blog.csdn.net/qq_43594278/article/details/116018869)

[Lua 最全的快速入门教程](https://panguangyu.blog.csdn.net/article/details/88775896)
