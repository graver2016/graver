# graver

## 参考资料
### youtube 教程
**c 语言实现**
[Make a compiler](https://www.youtube.com/playlist?list=PLvaIU2QC2uvFnVxXe-XzXJfd4dXGz5qBB)
[Building a Compiler in C](https://www.youtube.com/playlist?list=PLRnI_2_ZWhtA_ZAzEa8uJF8wgGF0HjjEz)
[Compiler (for fun, from scratch :p) | 2022](https://www.youtube.com/playlist?list=PLysa8wRFCssxGKj_RxBWr3rwmjEYlJIpa)

**c++ 实现**
[Basecode](https://www.youtube.com/playlist?list=PLWMUVtnFsZu6YAU-Fwva_Raz6-IbPd-2U)

### 编辑器
[Neovim from Scratch](https://www.youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ)

#### emacs 基本配置
```lisp
;;; emacs 原生配置

; 关闭菜单栏
(menu-bar-mode -1)
; 关闭工具栏
(tool-bar-mode -1)
; 关闭滚动条
(scroll-bar-mode -1)
; 关闭启动界面
(setq inhibit-startup-screen t)

;;; 防止页面滚动时跳动，
; scroll-margin 3 可以在靠近屏幕边沿3行时就开始滚动
; scroll-step 1 设置为每次翻滚一行，可以使页面更连续
(setq scroll-step 1 scroll-margin 3 scroll-conservatively 10000)

;;; 关闭出错量的提示音
(setq visible-bell t)

;;; ---禁止备份
(setq make-backup-files nil)

;;; 不生成临时文件
(setq-default make-backup-files nil)

;;; 允许emacs和外部其他程序的粘贴
(setq x-select-enable-clipboard t)

;;; ido的配置
; 这个可以使你在用C-x C-f打开文件的时候在后面有提示;
; 这里是直接打开了ido的支持，在emacs23中这个是自带的.
(ido-mode t)
; ido模式中不保存目录列表,解决退出Emacs时ido要询问编码的问题。
(setq ido-save-directory-list-file nil)

;;; 括号匹配
; 打开括号匹配显示模式
(show-paren-mode t)
; 括号匹配时可以高亮显示另外一边的括号，但光标不会烦人的跳到另一个括号处
(setq show-paren-style 'parenthesis)

;;; 设置字体
;;; 设置字体
(set-face-attribute 'default nil
                    :family "JetBrains Mono"
                    :height 100
                    :weight 'normal
                    :width 'normal)


;;; 在行首 C-k 时，同时删除该行
(setq-default kill-whole-line t)

;;; 启动时全屏
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;;; 右侧基准线
(setq-default fill-column 120)
(global-display-fill-column-indicator-mode t)


;;; 自定义快捷键
; Ctrl+Home 
; 跳到文件开头
(defun my/goto-beginning-of-buffer ()
  "Move cursor to the beginning of the buffer."
  (interactive)
  (beginning-of-buffer))
(global-set-key (kbd "C-<home>") 'my/goto-beginning-of-buffer)

; Ctrl+ENd
; 跳到文件末尾
(defun my/goto-end-of-buffer ()
  "Move cursor to the end of the buffer."
  (interactive)
  (end-of-buffer))
(global-set-key (kbd "C-<end>") 'my/goto-end-of-buffer)


;;; 国内源
(setq package-archives '(
        ("gnu"    . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
        ("nongnu" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/nongnu/")
        ("melpa"  . "https://melpa.org/packages/")
        ("melpa-tsinghua"  . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")
    )
)

;;; 刷新软件源索引
; 不检查签名
; 个别时候会出现签名校验失败
(setq package-check-signature nil) 

(require 'package)

; 初始化包管理器
(unless (bound-and-true-p package--initialized) 
    (package-initialize)
) 

; 刷新软件源索引
(unless package-archive-contents
    (package-refresh-contents)
)

; use package
(unless 
    (package-installed-p 'use-package) 
    (package-refresh-contents) 
    (package-install 'use-package)
)

;;; crux
(use-package crux 
    :ensure t
    ; 删除当前行
    :bind ("C-c k" . crux-smart-kill-line)
)

;;; 上下移动行
(use-package drag-stuff 
    :ensure t
    :bind (
        ("<M-up>". drag-stuff-up) 
        ("<M-down>" . drag-stuff-down)
    )
)

;;; 显示行号
(use-package emacs 
    :config 
        (setq display-line-numbers-type 't) 
        (global-display-line-numbers-mode t)
)

;;; 用y/n来代替yes/no
(use-package emacs 
    :config (defalias 'yes-or-no-p 'y-or-n-p)
)

;;; 主题
(use-package gruvbox-theme 
    :ensure t
    :init (load-theme 'gruvbox-light-soft t)
)
;(use-package solarized-theme
;    :ensure t
;    :config
;        (load-theme 'solarized-dark t)
;)

;;; 终端模拟器
(use-package vterm
    :ensure t)


; mode line
(use-package smart-mode-line 
    :ensure t
    :init 
        (setq 
            sml/no-confirm-load-theme t
            sml/theme 'respectful
        )
        (sml/setup)
)

;;; ivy-counsel-swiper 三剑客
(use-package ivy 
    :ensure t
    :defer 1 
    :demand 
    :hook (after-init . ivy-mode) 
    :config 
        (ivy-mode 1) 
        (setq
            ivy-use-virtual-buffers t 
            ivy-initial-inputs-alist nil 
            ivy-count-format "%d/%d " 
            enable-recursive-minibuffers t 
            ivy-re-builders-alist '(
                (t . ivy--regex-ignore-order)
            )
        )
)

; 优化控制栏显示方式
(use-package counsel 
    :ensure t
    :after (ivy) 
    :bind(
        ("M-x" . counsel-M-x) 
        ("C-x C-f" . counsel-find-file) 
        ("C-c f" . counsel-recentf)
        ("C-c g" . counsel-git)
    )
) 

; 更方便的搜索
(use-package swiper 
    :ensure t
    :after ivy 
    :bind(
        ("C-s" . swiper) 
        ("C-r" . swiper-isearch-backward)
    )

    :config (
        setq
            swiper-action-recenter t 
            swiper-include-line-number-in-search t
    )
)

;;; 快速切换窗口
(use-package ace-window 
    :ensure t
    :bind (
        ("M-o" . 'ace-window)
    )
)

(setq exec-path (append '("/home/laolang/.nvm/versions/node/v18.19.0/bin/node") exec-path))

;;; 设置 c/c++ tag 宽度
(setq-default c-basic-offset 4)
;;; 启动 .dir-locals.el
(setq enable-local-variables t)

;;; lsp-mode
(use-package lsp-mode
    :ensure t
    :init
        (setq lsp-clangd-binary-path "/usr/bin/clangd")
    :config
        (setq lsp-prefer-flymake nil)
    :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
            (c-mode . lsp)
            (c++-mode . lsp)
            ;; if you want which-key integration
            (lsp-mode . lsp-enable-which-key-integration)
        )
    :commands lsp
)

(use-package treemacs
    :ensure t
    :commands treemacs
)
(use-package lsp-treemacs
    :ensure t
    :commands lsp-treemacs-errors-list
    :after (lsp treemacs)
)


(use-package company
    :ensure t
    :config
        (setq company-minimum-prefix-length 1) ; 只需敲 1 个字母就开始进行自动补全
        (setq company-tooltip-align-annotations t)
        (setq company-idle-delay 0.0)
        (setq company-show-numbers t) ;; 给选项编号 (按快捷键 M-1、M-2 等等来进行选择).
        (setq company-selection-wrap-around t)
        ;(setq company-transformers '(company-sort-by-occurrence)) ; 根据选择的频率进行排序，读者如果不喜欢可以去掉
) 

(use-package company-box
    :ensure t
    :hook (company-mode . company-box-mode)
)

(add-hook 'c++-mode-hook
    (lambda ()
        (company-mode 1)))

(add-hook 'c-mode-hook
    (lambda ()
        (company-mode 1)))

(use-package lsp-ui
    :ensure t
    :commands lsp-ui-mode
    :config
        (setq   lsp-ui-sideline-enable nil ; 禁用lsp-ui的侧边栏提示
                lsp-ui-doc-enable t ; 启用lsp-ui的文档显示功能
                lsp-ui-doc-position 'at-point ; 将文档显示在光标所在位置
                lsp-ui-doc-header t ; 显示文档的标题
                lsp-ui-doc-include-signature t ; 显示文档的函数签名信息
                lsp-ui-peek-enable t ; 启用lsp-ui的Peek功能, 即查看定义和引用
                lsp-ui-peek-always-show t ; 始终显示Peek结果
        )
)

(add-hook 'before-save-hook #'lsp-format-buffer) ; 保存文件之前对缓冲区进行格式化
(add-hook 'c-mode-hook #'lsp-deferred)
(add-hook 'c++-mode-hook #'lsp-deferred)

(use-package flycheck
    :ensure t
    :config
        (setq truncate-lines nil) ; 如果单行信息很长会自动换行
    :hook
        (prog-mode . flycheck-mode)
)

(use-package flycheck-clang-tidy
    :ensure t
    :after flycheck
    :hook 
        (flycheck-mode . flycheck-clang-tidy-setup)
    :config
        (setq flycheck-clang-tidy-executable "/usr/bin/clang-tidy")
)

(use-package projectile
    :ensure t
    :bind (("C-c p" . projectile-command-map))
    :config
        (setq projectile-mode-line "Projectile")
        (setq projectile-track-known-projects-automatically nil)
)
(projectile-mode +1)
(setq projectile-startup-screen 'projectile)

(use-package counsel-projectile
    :ensure t
    :after (projectile)
    :init (counsel-projectile-mode)
)

(use-package dap-mode
    :ensure t
    :hook
        (lsp-mode . dap-mode)
        (lsp-mode . dap-ui-mode)
    :custom
        (dap-auto-configure-features (list 'sessions 'locals 'controls 'tooltip))
    :config
        (dap-auto-configure-mode)
        ; (require 'dap-lldb)
        (require 'dap-gdb-lldb)
        ; (dap-gdb-lldb-setup)
)


;;; 设置启动页
(use-package dashboard
    :ensure t
    :config
        (dashboard-setup-startup-hook)
)
(setq dashboard-banner-logo-title "潼关路上始终是黑暗的")
(setq dashboard-startup-banner "~/dashboard.png")
(setq projectile-startup-screen 'projectile)
(setq dashboard-items '((recents  . 10)
                        (projects . 10)
                        ))




(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(doom-themes lsp-treemacs use-package smart-mode-line lsp-mode gruvbox-theme drag-stuff crux counsel ace-window)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


```


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/graver2016/graver.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/graver2016/graver/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
