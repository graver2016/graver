#include <fmt/core.h>

#include <iostream>
#include <list>
#include <memory>

#include "doctest/doctest.h"
#include "graver/util/log_util.h"

using namespace graver;

TEST_SUITE("test_common") {
    TEST_CASE("test_common_one") {
        std::shared_ptr<spdlog::logger> log = LogUtil::getLogger("test");
        SPDLOG_LOGGER_INFO(log, fmt::format("test common one"));
    }
}