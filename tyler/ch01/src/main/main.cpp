#include <fmt/core.h>
#include <spdlog/common.h>

#include <iostream>
#include <memory>

#include "graver/configuration/graver_config.h"
#include "graver/util/log_util.h"

using namespace graver;

int main() {
    LogUtil::init(spdlog::level::info, "../logs/app.log");
    std::shared_ptr<spdlog::logger> log = LogUtil::getLogger("app");
    SPDLOG_LOGGER_INFO(log, "Hello Graver!");

    auto config = Configuration::getInstance();
    SPDLOG_LOGGER_INFO(log, fmt::format("graver version major is : {}", config->graverVersionMajor()));
    SPDLOG_LOGGER_INFO(log, fmt::format("graver version major is : {}", config->graverVersionMinor()));
    SPDLOG_LOGGER_INFO(log, fmt::format("graver version patch is : {}", config->graverVersionPatch()));
    return 0;
}