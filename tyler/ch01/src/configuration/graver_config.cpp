#include "graver/configuration/graver_config.h"

#include <fmt/core.h>

#include <cstdlib>
#include <memory>
#include <mutex>

#include "graver/configuration/configuration.h"

namespace graver {

using namespace internal::configuration;

static std::once_flag instanceOnceFlag;
static Configuration* instance = nullptr;

Configuration::Configuration() {
    this->m_graver_version_major = static_cast<int>(strtol(GRAVER_VERSION_STR2(GRAVER_VERSION_MAJOR), nullptr, 10));
    this->m_graver_version_major = static_cast<int>(strtol(GRAVER_VERSION_STR2(GRAVER_VERSION_MINOR), nullptr, 10));
    this->m_graver_version_major = static_cast<int>(strtol(GRAVER_VERSION_STR2(GRAVER_VERSION_PATCH), nullptr, 10));
    this->m_graver_version       = fmt::format("{}.{}.{}", this->m_graver_version_major, this->m_graver_version_minor,
                                               this->m_graver_version_patch);
}

Configuration* Configuration::getInstance() {
    std::call_once(instanceOnceFlag, initInstance);
    return instance;
}

void Configuration::initInstance() {
    instance = new Configuration();
}

int Configuration::graverVersionMajor() const {
    return this->m_graver_version_major;
}
int Configuration::graverVersionMinor() const {
    return this->m_graver_version_minor;
}
int Configuration::graverVersionPatch() const {
    return this->m_graver_version_patch;
}

std::string Configuration::graverVersion() const {
    return this->m_graver_version;
}

};  // namespace graver