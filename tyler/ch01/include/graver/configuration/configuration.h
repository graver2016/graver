#pragma once

namespace graver {
// clang-format off
#define GRAVER_VERSION_MAJOR 1
#define GRAVER_VERSION_MINOR 0
#define GRAVER_VERSION_PATCH 0
// clang-format on

};  // namespace graver
