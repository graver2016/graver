#pragma once

#include <string>

#include "graver/configuration/configuration.h"

namespace graver {

namespace internal {
namespace configuration {
#define GRAVER_VERSION_MAJOR_COPY (GRAVER_VERSION_MAJOR)
#define GRAVER_VERSION_MINOR_COPY (GRAVER_VERSION_MINOR)
#define GRAVER_VERSION_PATCH_COPY (GRAVER_VERSION_PATCH)

#define GRAVER_VERSION_STR(R) #R
#define GRAVER_VERSION_STR2(R) GRAVER_VERSION_STR(R)
};  // namespace configuration
};  // namespace internal

class Configuration {
public:
    [[nodiscard]] int         graverVersionMajor() const;
    [[nodiscard]] int         graverVersionMinor() const;
    [[nodiscard]] int         graverVersionPatch() const;
    [[nodiscard]] std::string graverVersion() const;

private:
    int         m_graver_version_major{0};
    int         m_graver_version_minor{0};
    int         m_graver_version_patch{0};
    std::string m_graver_version;

public:
    Configuration(Configuration&)             = delete;
    Configuration& operator=(Configuration&)  = delete;
    Configuration(Configuration&&)            = delete;
    Configuration& operator=(Configuration&&) = delete;

public:
    static Configuration* getInstance();

private:
    Configuration();

    static void initInstance();
};
};  // namespace graver