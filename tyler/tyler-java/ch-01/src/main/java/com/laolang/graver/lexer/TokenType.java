package com.laolang.graver.lexer;

/**
 * Token 类型.
 *
 * @author laolang
 * @version 0.1
 */
public enum TokenType {
    /**
     * 非法字符.
     */
    BAD,
    /**
     * 文件结束.
     */
    EOF,
    /**
     * 分号: ;.
     */
    SEMI,
    /**
     * 加号: +.
     */
    PLUS,
    /**
     * 星号: *.
     */
    STAR,
    /**
     * 左括号: (.
     */
    LP,
    /**
     * 右括号: ).
     */
    RP,
    /**
     * 数字.
     */
    NUMBER,
    /**
     * 非法数字.
     */
    BAD_NUMBER
}
