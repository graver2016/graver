package com.laolang.graver.lexer;

import lombok.Data;

/**
 * Token.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class Token {

    private TokenType tokenType;
    private String text;

    public Token(TokenType tokenType, String text) {
        this.tokenType = tokenType;
        this.text = text;
    }
}
