package com.laolang.graver;

import lombok.extern.slf4j.Slf4j;

/**
 * 此法解析器入门.
 * <p></p>
 * <p>视频: https://www.bilibili.com/video/BV1Xk4y1m7bm/?p=2</p>
 * <p>博客: https://blog.csdn.net/tyler_download/article/details/50668983</p>
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
public class Graver {

    public static void main(String[] args) {
        log.info("graver is running...");
    }
}
