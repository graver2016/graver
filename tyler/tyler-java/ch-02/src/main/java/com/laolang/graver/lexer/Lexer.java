package com.laolang.graver.lexer;

import cn.hutool.core.util.StrUtil;
import java.util.List;
import java.util.function.Predicate;
import org.testng.collections.Lists;

/**
 * 词法分析器.
 *
 * @author laolang
 * @version 0.1
 */
public class Lexer {

    /**
     * 输入文本.
     */
    private final String text;
    /**
     * 输入文本的 char 数组.
     */
    private final char[] chars;
    /**
     * 当前位置.
     */
    private int position = 0;
    /**
     * 当前字符.
     */
    private char currentChar = '\0';

    public Lexer(String text) {
        this.text = text;
        if (StrUtil.isEmpty(this.text)) {
            this.chars = new char[0];
        } else {
            this.chars = this.text.toCharArray();
            currentChar = chars[0];
        }
    }

    public List<Token> lex() {
        List<Token> tokens = Lists.newArrayList();

        // 空输入直接返回一个结束 Token
        if (StrUtil.isEmpty(text)) {
            tokens.add(new Token(TokenType.EOF, null));
            return tokens;
        }

        while (currentChar != '\0') {
            // 跳过空白字符
            if (Character.isWhitespace(currentChar)) {
                next();
                continue;
            }
            // 分号
            if (';' == currentChar) {
                tokens.add(new Token(TokenType.SEMI, ";"));
                next();
                continue;
            }

            // 加号
            if ('+' == currentChar) {
                tokens.add(new Token(TokenType.PLUS, "+"));
                next();
                continue;
            }

            // 乘号
            if ('*' == currentChar) {
                tokens.add(new Token(TokenType.STAR, "*"));
                next();
                continue;
            }

            // 左括号
            if ('(' == currentChar) {
                tokens.add(new Token(TokenType.LP, "(;)"));
                next();
                continue;
            }

            // 右括号
            if (')' == currentChar) {
                tokens.add(new Token(TokenType.RP, ")"));
                next();
                continue;
            }

            // 当前非数字标识符能否继续处理
            Predicate<Character> canContinue = character ->
                !Character.isWhitespace(currentChar) && '\0' != currentChar && isIdentifierChar(currentChar);

            // 如果不是数字,则认为是非法标识符
            if (!Character.isDigit(currentChar)) {
                StringBuilder sb = new StringBuilder();
                while (canContinue.test(currentChar)) {
                    sb.append(currentChar);
                    next();
                }
                tokens.add(new Token(TokenType.BAD, sb.toString()));
                continue;
            }
            // 以 0 开头的数字认为是非法数字
            if (currentChar == '0') {
                StringBuilder sb = new StringBuilder();
                while (canContinue.test(currentChar)) {
                    sb.append(currentChar);
                    next();
                }
                tokens.add(new Token(TokenType.BAD_NUMBER, sb.toString()));
            } else {
                // 解析数字
                StringBuilder sb = new StringBuilder();
                while (Character.isDigit(currentChar) && '\0' != currentChar) {
                    sb.append(currentChar);
                    next();
                }
                tokens.add(new Token(TokenType.NUMBER, sb.toString()));
            }
        }

        // 添加结束 Token
        tokens.add(new Token(TokenType.EOF, null));
        return tokens;
    }

    /**
     * 检查当前字符是否可以作为标识符.
     *
     * @param ch 当前字符
     * @return 是否可为标识符
     */
    private boolean isIdentifierChar(char ch) {
        return Character.isDigit(ch) || Character.isLetter(ch) || '_' == ch;
    }

    /**
     * 移动当前字符到下一个位置.
     */
    private void next() {
        position++;
        if (position >= chars.length) {
            currentChar = '\0';
        } else {
            currentChar = chars[position];
        }
    }


}
