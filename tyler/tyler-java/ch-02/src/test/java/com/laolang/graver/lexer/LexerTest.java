package com.laolang.graver.lexer;

import cn.hutool.core.collection.CollUtil;
import java.util.List;
import org.testng.annotations.Test;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public class LexerTest {

    @Test
    public void testOne() {
        String text = "1+2*3";
        Lexer lexer = new Lexer(text);
        List<Token> tokens = lexer.lex();
        if (CollUtil.isNotEmpty(tokens)) {
            for (Token token : tokens) {
                System.out.println(token.toString());
            }
        }
    }

}
